//If-Else
var nama = "John"
var peran = ""

if (nama == "") {
	console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
	console.log ("Nama harus diisi!")
}
else if (peran == "") {
	console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
	console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
}
else if (peran == "Penyihir") {
	console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
	console.log("Selamat datang di Dunia Werewolf," + nama)
	console.log("Halo Penyihir " + nama + ", Kamu dapat melihat siapa yang menjadi werewolf!")
} 
else if (peran == "Guard") {
	console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
	console.log("Selamat datang di Dunia Werewolf," + nama)
	console.log("Halo Guard " + nama + ", Kamu akan membantu melindungi temanmu dari serangan werewolf.")
} 
else if (peran == "Werewolf") {
	console.log("// Output untuk Input nama = " + nama + " dan peran = " + peran)
	console.log("Selamat datang di Dunia Werewolf," + nama)
	console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!")
} 
else{
	console.log("End")
}

//Switch-Case
var hari = 21; 
var bulan = 1; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';

if (hari<1 || hari > 31) {
	console.log("hari yang Anda inputkan tidak sesuai")
}
else{
	console.log("\nTanggal : " + hari)
}

console.log("Bulan : " + bulan)
switch(bulan){
	case 1 :
		bulan2 = "Januari";
		break;
	case 2 :
		bulan2 = "Februari";
		break;
	case 3 :
		bulan2 = "Maret";
		break;
	case 4 :
		bulan2 = "April";
		break;
	case 5 :
		bulan2 = "Mei";
		break;
	case 6 :
		bulan2 = "Juni";
		break;
	case 7 :
		bulan2 = "Juli";
		break;
	case 8 :
		bulan2 = "Agustus";
		break;
	case 9 :
		bulan2 = "september";
		break;
	case 10 :
		bulan2 = "Oktober";
		break;
	case 11 :
		bulan2 = "November";
		break;
	case 12 :
		bulan2 = "Desember";
		break;
	Default : 
	console.log("bulan yang Anda inputkan tidak sesuai")
}

if (tahun<1900 || tahun > 2200) {
	console.log("tahun yang Anda inputkan tidak sesuai")
}
else{
	console.log("Tahun : " + tahun)
}

console.log("\n" + hari + " " + bulan2 + " " + tahun)