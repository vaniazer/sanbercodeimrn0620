//Looping-While
var flag = 2;
var flag2 = 20;
console.log("LOOPING PERTAMA")
while(flag <= 20) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag + "- I love coding"); // Menampilkan nilai flag pada iterasi tertentu
  flag = flag + 2; 
}
console.log("LOOPING KEDUA")
while(flag2 >= 2) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log(flag2 + "- I will become a mobile developer"); // Menampilkan nilai flag pada iterasi tertentu
  flag2 = flag2 - 2; 
}

//Looping-For
for (var i = 1; i <= 20; i++) {
	if( i%3 == 0) {
		if (i%2==0) {
			console.log(i + " - Berkualitas")
		}
		else{
			console.log(i + " - I Love Coding")
		}
	}
	else if (i%2!=0){
		console.log(i + " - Santai")
	}
	else if(i%3!=0) {
		console.log(i + " - Berkualitas")
	} 
	
}

//Persegi Panjang
var kalimat=""

	for (var p = 1; p <= 8; p++) {
		kalimat=kalimat+"#" 
	} 
	for (var l = 1; l <= 4; l++){
			console.log(kalimat)
	} 

//Membuat Tangga
var kal=""

	for (var pa = 1; pa <= 7; pa++) {
		for (var la = 0; la < pa; la++){
			kal=kal+"#" 
		} 
	console.log(kal)
	kal=""
	} 

//Membuat Papan Catur
var kali=""
for (var var1 = 0 ; var1 < 8; var1++){
	for (var var2 = 0;var2 < 8; var2++) {
		if (var1%2==0) {
			if (var2%2!=0) {
				kali=kali+"#" 
			}
			else{
				kali=kali+" " 
			}
		}else{
			if (var2%2==0) {
				kali=kali+"#" 
			}
			else{
				kali=kali+" " 
			}
		}
	}
	console.log(kali)
	kali = ""
}


