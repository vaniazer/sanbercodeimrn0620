//No.1
function teriak() {
  console.log("Halo Sanbers!");
}
 
teriak(); 

//No.2
var num1 = 12
var num2 = 4
function kalikan(num1,num2) {
  return num1 * num2 
}
 
var hasilKali = kalikan(num1,num2)
console.log("\n" + hasilKali) 

//No.3
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
function introduce(name,age,address,hobby) {
  return "\nNama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address +", dan saya punya hobby yaitu " + hobby +"! "
}

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)