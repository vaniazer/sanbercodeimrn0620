//Animal Case
class Animal {
  constructor(name) {
    this.name = name;
    this.legs = 4
    this.cold_blooded = false
  }
 }
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false



class Ape {
  constructor(name) {
    this.name = name;
  }

  yell(){
  	return "Auooo";
  }
 }

class Frog {
  constructor(name) {
    this.name = name;
  }

  jump(){
  	return "hop hop";
  }
}
console.log("\n")
var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 


//Function to Class
class Clock{
	constructor({ template }) {
		var date = new Date();

	    this.hours = date.getHours();
	    if (this.hours < 10) {
	    	this.hours = '0' + this.hours;
		}
	    this.mins = date.getMinutes();
	    if (this.mins < 10) {
	    	this.mins = '0' + this.mins;
	    }

	    this.secs = date.getSeconds();
	    if (this.secs < 10) {
	    	this.secs = '0' + this.secs;
	    }

	    this.output = template
	      .replace('h', this.hours)
	      .replace('m', this.mins)
	      .replace('s', this.secs);
	  }
  render(){
    console.log(this.output);
  }

  stop(){
    clearInterval(this.timer);
  }

  start(){
    this.render();
    this.timer = setInterval(this.render, 1000);
  };

}


var clock = new Clock({template: 'h:m:s'});
clock.start(); 