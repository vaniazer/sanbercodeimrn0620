//No.1
 var people = [ ["Bruce", "Banner", "male", 1975], 
["Natasha", "Romanoff", "female"],
["Tony", "Stark", "male", 1980],
["Pepper", "Pots", "female", 2023] ]

arrayToObject(people) 
    var now = new Date();
    var thisYear = now.getFullYear();
   
function arrayToObject(arr) {
    
    for (var i = 0; i < people.length; i++) {
        console.log("\n")
        console.log('firstName : ' + people[i][0])
        console.log('lastName : ' + people[i][1])
        console.log('gender : ' + people[i][2])
       if (people[i][3]==null || people[i][3]>=2020) {
        console.log('age : Invalid Birth Year')
        }
        else{
        console.log('age : ' + people[i][3])
        }
    }
    
    
}

 
// Driver Code

/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

 //No.2
function makeObj (memberID, money) {
  this.memberID = memberID;
  this.money = money;
  this.listPurchased = [];
  this.changeMoney = 0;
}
/*
var shoppingObj = {
  memberID: 'a',
  money: 0,
  listPurchased: [],
  changeMoney: 0
};*/

function shoppingTime(memberId, money) {
  if (memberId === '' || (memberId === undefined && money === undefined)) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } 
  else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  } 
  else {
    shoppingObj = new makeObj(memberId, money);
    shoppingObj.memberID = memberId;
    shoppingObj.money = money;
    var canPurchase = true;
    while (money > 0 && canPurchase) {
      if (money >= 1500000) {
        shoppingObj.listPurchased.push('Sepatu Stacattu');
        money -= 1500000;
        canPurchase = true;
      }
      if (money >= 500000) {
        shoppingObj.listPurchased.push('Baju Zoro');
        money -= 500000;
        canPurchase = true;
      }
      if (money >= 250000) {
        shoppingObj.listPurchased.push('Baju H&N');
        money -= 250000;
        canPurchase = true;
      }
      if (money >= 175000) {
        shoppingObj.listPurchased.push('Sweater Uniklooh');
        money -= 175000;
        canPurchase = true;
      }
      if (money >= 50000) {
        shoppingObj.listPurchased.push('Casing Handphone');
        money -= 50000;
        canPurchase = true;
      }
      canPurchase = false;
    }
    shoppingObj.changeMoney = money;
    return shoppingObj;
  }
}
 
// TEST CASES
console.log("\n")
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log("\n")
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//No.3
function naikAngkot(arrPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var biaya = 2000;
  var hasil = [];

  if (arrPenumpang.length == 0) {
    return arrPenumpang;
  }

  for (var i = 0; i < arrPenumpang.length; i++) {
      var penumpang = arrPenumpang[i];
      var objPenumpang = {};

      objPenumpang.penumpang = penumpang[0];
      objPenumpang.naikDari = penumpang[1];
      objPenumpang.tujuan = penumpang[2];
      objPenumpang.bayar = biaya * (rute.indexOf(objPenumpang.tujuan) - rute.indexOf(objPenumpang.naikDari));

      hasil.push(objPenumpang);
  }
return hasil
}

//TEST CASE
console.log("\n")
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]