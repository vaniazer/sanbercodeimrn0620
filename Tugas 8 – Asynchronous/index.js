// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
// Menjalankan function callback yang sebelumnya sudah dideklarasi
var time = 10000;
function penghitung(x){
	if (x == books.length) {
		return 0
	}
	else{
		readBooks(time,books[x], function(check) {
			time = books[x].timeSpent;
			penghitung(x+1)
		})
	}
}
penghitung(0) 